module n_auth

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	go.mongodb.org/mongo-driver v1.5.0
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073
)
